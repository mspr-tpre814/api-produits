package com.mspr.apiproducts.api.mapper;

import com.mspr.apiproducts.api.dto.DetailsDTO;
import com.mspr.apiproducts.api.dto.ProductRequestDTO;
import com.mspr.apiproducts.api.dto.ProductResponseDTO;
import com.mspr.apiproducts.entities.Details;
import com.mspr.apiproducts.entities.Product;
import org.springframework.stereotype.Component;

/**
 * Mapper class to convert ProductRequestDTO to Product and Product to ProductResponseDTO
 */

@Component
public class Mapper {
    /**
     * Convert Product to ProductResponseDTO
     * @param product Product
     * @return ProductResponseDTO
     */
    public ProductResponseDTO toDTO(Product product) {
        return new ProductResponseDTO(
                product.getId(),
                product.getCreatedAt(),
                product.getName(),
                toDTO(product.getDetails()),
                product.getStock()
        );
    }

    /**
     * Convert ProductRequestDTO to Product
     * @param productRequestDTO ProductRequestDTO
     * @return Product
     */
    public Product toEntity(ProductRequestDTO productRequestDTO) {
        return new Product(
                productRequestDTO.getName(),
                toEntity(productRequestDTO.getDetails()),
                productRequestDTO.getStock()
        );
    }

    /**
     * Convert Details to DetailsDTO
     * @param details Details
     * @return DetailsDTO
     */
    public DetailsDTO toDTO(Details details) {
        return new DetailsDTO(
                details.getPrice(),
                details.getDescription(),
                details.getColor()
        );
    }

    /**
     * Convert DetailsDTO to Details
     * @param detailsDTO DetailsDTO
     * @return Details
     */
    public Details toEntity(DetailsDTO detailsDTO) {
        return new Details(
                detailsDTO.getPrice(),
                detailsDTO.getDescription(),
                detailsDTO.getColor()
        );
    }
}
