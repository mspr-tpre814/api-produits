package com.mspr.apiproducts.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

/**
 * ProductResponseDTO
 * ProductResponseDTO is the DTO used to return a product
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponseDTO {
    private String id;
    private Instant createdAt;
    private String name;
    private DetailsDTO details;
    private int stock;

}
