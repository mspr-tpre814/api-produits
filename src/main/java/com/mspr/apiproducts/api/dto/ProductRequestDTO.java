package com.mspr.apiproducts.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ProductRequestDTO
 * ProductRequestDTO is the DTO used to create a product
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequestDTO {
    private String name;
    private DetailsDTO details;
    private int stock;
}
