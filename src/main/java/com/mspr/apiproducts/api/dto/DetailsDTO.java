package com.mspr.apiproducts.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DetailsDTO
 * DetailsDTO is the DTO used to return details of a product
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetailsDTO {
    private String price;
    private String description;
    private String color;
}
