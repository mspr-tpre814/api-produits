package com.mspr.apiproducts.api.controllers;

import com.mspr.apiproducts.api.dto.ProductRequestDTO;
import com.mspr.apiproducts.api.dto.ProductResponseDTO;
import com.mspr.apiproducts.api.mapper.Mapper;
import com.mspr.apiproducts.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * ProductController
 * This class is the controller for the Product entity.
 * It handles the CRUD operations for the Product entity.
 * It is accessible from the /products path.
 *
 * @RestController annotation is used to create RESTful web services using Spring MVC.
 *
 */

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/products", produces = APPLICATION_JSON_VALUE)
public class ProductController {
    private final ProductService productService;
    private final Mapper mapper;

    /**
     * This method returns all the products.
     *
     * @return ResponseEntity<List<ProductResponseDTO>> This returns the list of products.
     */
    @GetMapping("/")
    public ResponseEntity<List<ProductResponseDTO>> getAllProducts() {
        List<ProductResponseDTO> products = productService.getProducts()
                .stream()
                .map(mapper::toDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    /**
     * This method returns a product by its id.
     *
     * @param id The id of the product.
     * @return ResponseEntity<ProductResponseDTO> This returns the product.
     */
    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDTO> getProduct(@PathVariable String id) {
        return new ResponseEntity<>(mapper.toDTO(productService.getProduct(id)), HttpStatus.OK);
    }

    /**
     * This method creates a product.
     *
     * @param productRequestDTO The product to create.
     * @return ResponseEntity<ProductResponseDTO> This returns the created product.
     */
    @PostMapping("/")
    public ResponseEntity<ProductResponseDTO> createProduct(@RequestBody ProductRequestDTO productRequestDTO) {
        return new ResponseEntity<>(mapper.toDTO(productService.createProduct(mapper.toEntity(productRequestDTO))), HttpStatus.CREATED);
    }

    /**
     * This method updates a product.
     *
     * @param id The id of the product.
     * @param productRequestDTO The product to update.
     * @return ResponseEntity<ProductResponseDTO> This returns the updated product.
     */
    @PutMapping("/{id}")
    public ResponseEntity<ProductResponseDTO> updateProduct(@PathVariable String id, @RequestBody ProductRequestDTO productRequestDTO) {
        return new ResponseEntity<>(mapper.toDTO(productService.updateProduct(id, mapper.toEntity(productRequestDTO))), HttpStatus.OK);
    }

    /**
     * This method deletes a product.
     *
     * @param id The id of the product.
     * @return ResponseEntity<?> This returns an empty response.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable String id) {
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
