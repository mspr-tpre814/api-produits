package com.mspr.apiproducts.services;

import com.mspr.apiproducts.entities.Product;

import java.util.List;

/**
 * ProductService
 * Service for Product entity
 */

public interface ProductService {
    List<Product> getProducts();
    Product getProduct(String id);
    Product createProduct(Product product);
    Product updateProduct(String uid, Product product);
    void deleteProduct(String id);
    boolean checkProductAvailability(String id);
    void updateProductStock(String id);
}
