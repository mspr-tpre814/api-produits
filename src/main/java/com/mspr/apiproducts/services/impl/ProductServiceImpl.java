package com.mspr.apiproducts.services.impl;

import com.mspr.apiproducts.entities.Product;
import com.mspr.apiproducts.repositories.DetailsRepository;
import com.mspr.apiproducts.repositories.ProductRepository;
import com.mspr.apiproducts.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

/**
 * ProductServiceImpl
 * ProductServiceImpl is the implementation of ProductService
 */

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private static final int QUANTITE = 1;
    private final ProductRepository productRepository;
    private final DetailsRepository detailsRepository;

    /**
     * Check if a product is available
     * @param id the product id
     * @return true if the product is available, false otherwise
     */
    @Override
    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    /**
     * Get a product by its id
     * @param id the product id
     * @return the product
     */
    @Override
    public Product getProduct(String id) {
        return productRepository.findById(id).orElseThrow();
    }

    /**
     * Create a product
     * @param product the product to create
     * @return the created product
     */
    @Override
    public Product createProduct(Product product) {
        product.setCreatedAt(Instant.now());
        detailsRepository.save(product.getDetails());
        return productRepository.save(product);
    }

    /**
     * Update a product
     * @param uid the product id
     * @param product the product to update
     * @return the updated product
     */
    @Override
    public Product updateProduct(String uid, Product product) {
        detailsRepository.save(product.getDetails());
        return productRepository.save(product);
    }

    /**
     * Delete a product
     * @param id the product id
     */
    @Override
    public void deleteProduct(String id) {
        productRepository.deleteById(id);
    }

    /**
     * Check if a product is available
     * @param id the product id
     * @return true if the product is available, false otherwise
     */
    @Override
    @RabbitListener(queues = "productAvailabilityQueue")
    public boolean checkProductAvailability(String id) {
        return productRepository.existsById(id);
    }

    /**
     * Update the stock of a product
     * @param id the product id
     */
    @Override
    @RabbitListener(queues = "productUpdateStockQueue")
    public void updateProductStock(String id) {
        Product product = productRepository.findById(id).orElseThrow();
        product.setStock(product.getStock() - QUANTITE);
        productRepository.save(product);
    }
}
