package com.mspr.apiproducts.repositories;

import com.mspr.apiproducts.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ProductRepository
 * Repository for Product entity
 */

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
}
