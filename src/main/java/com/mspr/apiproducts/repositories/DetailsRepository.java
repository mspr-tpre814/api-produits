package com.mspr.apiproducts.repositories;

import com.mspr.apiproducts.entities.Details;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * DetailsRepository
 * Repository for Details entity
 */

public interface DetailsRepository extends JpaRepository<Details, Integer> {
}
