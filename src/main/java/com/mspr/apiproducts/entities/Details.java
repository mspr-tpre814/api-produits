package com.mspr.apiproducts.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Details
 * Details is the entity used to store details of a product
 */

@Entity
@Table(name = "details")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Details {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "price")
    private String price;
    @Column(name = "description")
    private String description;
    @Column(name = "color")
    private String color;
    @OneToOne(mappedBy = "details")
    @JsonIgnore
    private Product product;

    public Details(String id, String price, String description, String color) {
        this.id = id;
        this.price = price;
        this.description = description;
        this.color = color;
    }

    public Details(String price, String description, String color) {
        this.price = price;
        this.description = description;
        this.color = color;
    }
}
