package com.mspr.apiproducts.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

/**
 * Product
 * Product is the entity used to represent a product
 */

@Entity
@Table(name = "product")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "created_at")
    private Instant createdAt;
    @Column(name = "name")
    private String name;
    @OneToOne
    @JoinColumn(name = "details_id")
    private Details details;
    @Column(name = "stock")
    private int stock;

    public Product(String name, Details entity, int stock) {
        this.name = name;
        this.details = entity;
        this.stock = stock;
    }
}
