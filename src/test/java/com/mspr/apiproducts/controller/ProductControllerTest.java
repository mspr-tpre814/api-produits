package com.mspr.apiproducts.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mspr.apiproducts.api.controllers.ProductController;
import com.mspr.apiproducts.api.dto.DetailsDTO;
import com.mspr.apiproducts.api.dto.ProductRequestDTO;
import com.mspr.apiproducts.api.dto.ProductResponseDTO;
import com.mspr.apiproducts.api.mapper.Mapper;
import com.mspr.apiproducts.entities.Details;
import com.mspr.apiproducts.entities.Product;
import com.mspr.apiproducts.services.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private Mapper mapper;

    private Product product;
    private ProductResponseDTO productResponseDTO;

    @Before
    public void setUp() {
        product = new Product("1", Instant.now(), "Product1", new Details("10", "Description", "Red"), 100);
        productResponseDTO = new ProductResponseDTO("1", Instant.now(), "Product1", new DetailsDTO("10", "Description", "Red"), 100);
    }

    @Test
    public void getAllProductsTest() throws Exception {
        List<Product> productList = Collections.singletonList(product);

        when(productService.getProducts()).thenReturn(productList);
        when(mapper.toDTO(product)).thenReturn(productResponseDTO);

        mockMvc.perform(get("/products/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(productResponseDTO.getId())))
                .andExpect(jsonPath("$[0].name", is(productResponseDTO.getName())));
    }

    @Test
    public void getProductTest() throws Exception {
        when(productService.getProduct(anyString())).thenReturn(product);
        when(mapper.toDTO(product)).thenReturn(productResponseDTO);

        mockMvc.perform(get("/products/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(productResponseDTO.getId())))
                .andExpect(jsonPath("$.name", is(productResponseDTO.getName())));
    }

    @Test
    public void createProductTest() throws Exception {
        when(productService.createProduct(any(Product.class))).thenReturn(product);
        when(mapper.toDTO(any(Product.class))).thenReturn(productResponseDTO);
        when(mapper.toEntity(any(ProductRequestDTO.class))).thenReturn(product);

        mockMvc.perform(post("/products/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productResponseDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(productResponseDTO.getId())))
                .andExpect(jsonPath("$.name", is(productResponseDTO.getName())));
    }

    @Test
    public void updateProductTest() throws Exception {
        when(productService.updateProduct(anyString(), any(Product.class))).thenReturn(product);
        when(mapper.toDTO(any(Product.class))).thenReturn(productResponseDTO);
        when(mapper.toEntity(any(ProductRequestDTO.class))).thenReturn(product);

        mockMvc.perform(put("/products/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productResponseDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(productResponseDTO.getId())))
                .andExpect(jsonPath("$.name", is(productResponseDTO.getName())));
    }

    @Test
    public void deleteProductTest() throws Exception {
        mockMvc.perform(delete("/products/1"))
                .andExpect(status().isOk());
    }

    private static String asJsonString(final Object obj) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
