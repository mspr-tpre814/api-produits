package com.mspr.apiproducts.services.impl;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.mspr.apiproducts.entities.Details;
import com.mspr.apiproducts.entities.Product;
import com.mspr.apiproducts.repositories.DetailsRepository;
import com.mspr.apiproducts.repositories.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private DetailsRepository detailsRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    private Product product;
    private Details details;

    @Before
    public void setUp() {
        details = new Details("1", "10", "Description", "Red");
        product = new Product("1", Instant.now(), "Product1", details, 100);
    }

    @Test
    public void getProductsTest() {
        when(productRepository.findAll()).thenReturn(Collections.singletonList(product));
        List<Product> products = productService.getProducts();
        assertFalse(products.isEmpty());
        assertEquals("Product1", products.get(0).getName());
    }

    @Test
    public void getProductTest() {
        when(productRepository.findById(anyString())).thenReturn(Optional.of(product));
        Product foundProduct = productService.getProduct("1");
        assertNotNull(foundProduct);
        assertEquals("Product1", foundProduct.getName());
    }

    @Test
    public void createProductTest() {
        when(detailsRepository.save(any(Details.class))).thenReturn(details);
        when(productRepository.save(any(Product.class))).thenReturn(product);

        Product createdProduct = productService.createProduct(product);
        assertNotNull(createdProduct);
        assertEquals("Product1", createdProduct.getName());
    }

    @Test
    public void updateProductTest() {
        when(detailsRepository.save(any(Details.class))).thenReturn(details);
        when(productRepository.save(any(Product.class))).thenReturn(product);

        Product updatedProduct = productService.updateProduct("1", product);
        assertNotNull(updatedProduct);
        assertEquals("Product1", updatedProduct.getName());
    }

    @Test
    public void deleteProductTest() {
        doNothing().when(productRepository).deleteById(anyString());
        productService.deleteProduct("1");
        verify(productRepository, times(1)).deleteById("1");
    }

    @Test
    public void checkProductAvailabilityTest() {
        when(productRepository.existsById(anyString())).thenReturn(true);
        boolean isAvailable = productService.checkProductAvailability("1");
        assertTrue(isAvailable);
    }

    @Test
    public void updateProductStockTest() {
        when(productRepository.findById(anyString())).thenReturn(Optional.of(product));
        productService.updateProductStock("1");
        verify(productRepository, times(1)).save(product);
        assertEquals(99, product.getStock());
    }

    @Test(expected = NoSuchElementException.class)
    public void getProductNotFoundTest() {
        when(productRepository.findById(anyString())).thenReturn(Optional.empty());
        productService.getProduct("1");
    }

    @Test
    public void createProductValidationErrorTest() {
        Product invalidProduct = new Product("", Instant.now(), "", new Details("", "", ""), 0);

        when(detailsRepository.save(any(Details.class))).thenReturn(details);
        when(productRepository.save(any(Product.class))).thenThrow(new DataIntegrityViolationException("Validation failed"));

        try {
            productService.createProduct(invalidProduct);
        } catch (DataIntegrityViolationException e) {
            assertEquals("Validation failed", e.getMessage());
        }
    }
}
